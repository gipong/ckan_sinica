#!/usr/bin/env python

import urllib2
import urllib
import json
import pprint
import requests
from osgeo import ogr
from osgeo import osr


data_string = urllib.quote(json.dumps({'id': 'ortho63'}))   # 以ckan資料集中ortho63作為搜尋對象
response = urllib2.urlopen('http://taijiang.tw/api/3/action/package_show',data_string)

response_dict = json.loads(response.read())
result = response_dict['result']['extras']


for item in range(len(result)): # 擷取該資料集空間欄位資訊
    if result[item]['key'] == 'spatial':
        print result[item]['value']
        geom = ogr.CreateGeometryFromJson(str(result[item]['value']))
        
print geom

searchres = urllib2.urlopen('http://taijiang.tw/api/3/action/package_list') # 針對台江內所有資料集進行比對

searchres_dict = json.loads(searchres.read())
dataset = searchres_dict['result']

print dataset

for data in dataset:
    data_id = urllib.quote(json.dumps({'id': str(data)}))
    response = urllib2.urlopen('http://taijiang.tw/api/3/action/package_show',data_id)
    response_dict = json.loads(response.read())
    result = response_dict['result']['extras']
    
    for item in range(len(result)):
        
        if result[item]['key'] == 'spatial':
            geom_cmp = ogr.CreateGeometryFromJson(str(result[item]['value']))

            if geom_cmp:
                if json.loads(str(result[item]['value']))['coordinates'][0][0][0] > 180:    # 預設非wgs84座標系統皆為twd67
                    #print json.loads(result[item]['value'])['coordinates']
                    source = osr.SpatialReference()
                    source.ImportFromEPSG(3826)                   
                    target = osr.SpatialReference()
                    target.ImportFromEPSG(4326)                
                    geom_cmp.Transform(osr.CoordinateTransformation(source, target))  
                    
                print str(data)+":"+str(geom.Intersect(geom_cmp)) # 檢查兩兩是否有Intersect
            
        