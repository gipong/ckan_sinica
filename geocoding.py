import requests
from bs4 import BeautifulSoup
from pyproj import Proj, transform

'''  
資料來源：地名檢索系統 http://placesearch.moi.gov.tw/search/

database：
alldb -> 所有資料庫
25k_2002 -> 經建三版兩萬五千分一地圖地名
5000_1 -> 臺灣五千分之一地圖地名

以下地名資料庫多沒有地理坐標
chen_quo -> 臺灣地名辭典--陳國章編
tw_fort -> 1904年台灣堡圖地名
ching -> 清代臺灣古地圖地名

'''

def convert2decimal(x, y):
    '''
    25°2'30N,121°22'18E -> 121.371666667 25.0416666667
    '''
    
    datax = [x.split(u'°')[0], x.split(u'°')[1].split('\'')[0], x.split(u'°')[1].split('\'')[1][:-1]]
    nx = float(datax[0]) + float(datax[1])/60 + float(datax[2])/3600
    if x.split(u'°')[1].split('\'')[1][-1:] == 'S':
        nx = -nx
        
    datay = [y.split(u'°')[0], y.split(u'°')[1].split('\'')[0], y.split(u'°')[1].split('\'')[1][:-1]]
    ny = float(datay[0]) + float(datay[1])/60 + float(datay[2])/3600
    if y.split(u'°')[1].split('\'')[1][-1:] == 'W':
        ny = -ny    
        
    return nx,ny

payload = {'database': 'alldb', 'keyword': '周厝', 'type': '', 'Submit': '%E9%80%81%E5%87%BA%E6%9F%A5%E8%A9%A2'}
r = requests.post("http://placesearch.moi.gov.tw/search/left2.php", data=payload)   # 送出查詢表單
soup = BeautifulSoup(r.text)

dbcount = len(soup.find_all('option'))
dbdata = soup.find_all('table')


for i in range(dbcount):
    if i == 0: continue
    if len(dbdata[i].find_all('td')) > 3:   #確認該資料庫有值
        firstLink = dbdata[i].find_all('td')[5].a['href']
        firstLinkDb = i
        break
    
if firstLink:
    r2 = requests.get("http://placesearch.moi.gov.tw/search/"+firstLink)    # 以查詢結果第一筆為例，並至該地名檢視頁面擷取地理坐標
    soup_result = BeautifulSoup(r2.text)
    
    
    if firstLinkDb == 1:    # 經建三版兩萬五千分一地圖地名
        geolocation = soup_result.find_all('table')[6].find_all('td')[7].text   
        x1,y1 = geolocation.split(',')
        
        inProj = Proj(init='epsg:3826')
        outProj = Proj(init='epsg:4326')
        
        x2,y2 = transform(inProj, outProj, x1.split(':')[1], y1.split(':')[1]) 
        print x2, y2
        
    elif firstLinkDb == 2:  # 臺灣五千分之一地圖地名
        geolocation = soup_result.find_all('table')[6].find_all('td')[7].text
        y1_temp,x1_temp = geolocation.split(',')
        x,y = convert2decimal(x1_temp, y1_temp)
        print x, y
        
    else:
        print 'No results.'
            
else:
    print 'No results.'
